# ファイル管理アプリ開発環境構築

0. docker-composeコマンド使用できるようにする

    https://docs.docker.com/get-docker/


1. git clone

    ```
    git clone git@bitbucket.org:yugo_mizuochi/mizuochi-file-app.git
    ```


2. コンテナ起動

    ```
    docker-compose up -d
    ```


3. アプリケーションコンテナに入る

    ```
    docker exec -it php-fpm bash
    ```


4. composerインストール

    ```
    composer install
    ```


5. envコピー

    ```
    cp .env.example .env
    ```


6. アプリケーションキー作成

    ```
    php artisan key:generate
    ```


7. migrate実行

    ```
    php artisan migrate
    ```


8. npmでパッケージインストール

    ```
    npm install
    ```


9. jsファイルビルド

    ```
    npm run dev
    ```