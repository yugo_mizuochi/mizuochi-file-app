@extends('general.common.templete')

@section('title', 'エラー')

@include('general.common.nav')

@section('content')

	<div id="container">

		<div id="main">

            <div class="error_contents">情報を取得できませんでした。</div>
            <div class="top_button_box"><a class="top_button" href="{{ route('general.home') }}">ホーム画面へ戻る</a></div>

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection