@extends('admin.common.templete')

@section('title', 'ユーザー一覧')

@include('admin.common.nav')

@include('common.footer')

@section('content')

	<link rel="stylesheet" type="text/css" href="{{ mix('css/admin/users/users.css') }}"> 

	<div id="container">

		<div id="main">

			<div id="app">
				<admin-users-component></admin-users-component>
			</div>

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection