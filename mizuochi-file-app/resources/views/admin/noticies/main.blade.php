@extends('admin.common.templete')

@section('title', 'お知らせ一覧')

@include('admin.common.nav')

@include('common.footer')

@section('content')

	<link rel="stylesheet" type="text/css" href="{{ mix('css/admin/noticies/noticies.css') }}"> 

	<div id="container">

		<div id="main">

			<div id="app">
				<admin-noticies-component></admin-noticies-component>
			</div>

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection