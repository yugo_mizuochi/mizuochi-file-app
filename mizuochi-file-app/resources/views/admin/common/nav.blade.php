@section('admin_nav')

	<div id="sub">

		<nav id="menubar">
			<ul>
                <li><a href="{{ route('general.home') }}">ホーム画面</a></li>
                <li><a href="{{ route('admin.noticies') }}">お知らせ一覧</a></li>
				<li><a href="{{ route('admin.users') }}">ユーザー一覧</a></li>
                <li><a href="{{ route('logout') }}">ログアウト</a></li>
			</ul>
		</nav>

	</div>
	<!--/sub-->

@endsection