@extends('general.common.templete')

@section('title', 'マイファイル一覧')

@include('general.common.nav')

@include('common.footer')

@section('content')

	<link rel="stylesheet" type="text/css" href="{{ mix('css/general/myfiles/myfiles.css') }}"> 

	<div id="container">

		<div id="main">

			<div id="app">
				<myfiles-component></myfiles-component>
			</div>

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection