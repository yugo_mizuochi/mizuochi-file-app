@extends('general.common.templete')

@section('title', 'ホーム画面')

@include('general.common.nav')

@include('common.footer')

@section('content')

	<link rel="stylesheet" type="text/css" href="{{ mix('css/general/home/home.css') }}">

	<div id="container">

		<div id="main">

			<div id="app">
				<noticies-component></noticies-component>
			</div>

			@component('general.home.introduction_box')@endcomponent

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection