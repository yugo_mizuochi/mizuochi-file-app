<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="UTF-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title')</title>
		<link rel="stylesheet" href="{{ mix('css/common/common.css') }}">
		<link rel="stylesheet" href="{{ mix('/css/app.css') }}">
	</head>

	<body>

		@yield('general_nav')

		@yield('content')

		@yield('footer')

		<script src="{{ mix('js/app.js') }}"></script>

	</body>
</html>
