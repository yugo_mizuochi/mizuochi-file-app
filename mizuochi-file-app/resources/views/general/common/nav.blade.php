@section('general_nav')

	<div id="sub">

		<nav id="menubar">
			<ul>
				@can('admin_user')
					<li><a href="{{ route('admin.noticies') }}">管理画面</a></li>
				@endcan
					<li><a href="{{ route('general.home') }}">ホーム画面</a></li>
					<li><a href="{{ route('general.my_files') }}">マイファイル一覧</a></li>
					<li><a href="{{ route('general.my_account') }}">マイアカウント</a></li>
					<li><a href="{{ route('logout') }}">ログアウト</a></li>
			</ul>
		</nav>

	</div>
	<!--/sub-->

@endsection