@extends('general.common.templete')

@section('title', 'マイアカウント')

@include('general.common.nav')

@include('common.footer')

@section('content')

	<link rel="stylesheet" type="text/css" href="{{ mix('css/general/myaccount/myaccount.css') }}"> 

	<div id="container">

		<div id="main">

			<div id="app">
				<my-account-component></my-account-component>
			</div>

		</div>
		<!--/main-->

	</div>
	<!--/container-->

@endsection