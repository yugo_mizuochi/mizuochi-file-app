/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('noticies-component', require('./components/General/Home/NoticiesComponent.vue').default);
Vue.component('myfiles-component', require('./components/General/MyFiles/MyFilesComponent.vue').default);
Vue.component('myfiles-menu-component', require('./components/General/MyFiles/MyFilesMenuComponent.vue').default);
Vue.component('my-account-component', require('./components/General/MyAccount/MyAccountComponent.vue').default);
Vue.component('admin-noticies-component', require('./components/Admin/Noticies/NoticiesComponent.vue').default);
Vue.component('admin-noticies-menu-component', require('./components/Admin/Noticies/NoticiesMenuComponent.vue').default);
Vue.component('admin-users-component', require('./components/Admin/Users/UsersComponent.vue').default);
Vue.component('admin-users-menu-component', require('./components/Admin/Users/UsersMenuComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
