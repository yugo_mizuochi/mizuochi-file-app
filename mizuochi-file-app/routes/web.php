<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::get('/', function () {
	    return view('auth.login');
	});

	//ログアウト
	Route::get('logout', 'Auth\LogoutController@logout')->name('logout');

	Auth::routes();

	// 全ユーザー遷移可能URL
	Route::group(['prefix' => 'general', 'as' => 'general.', 'middleware' => ['auth', 'can:all_user']], function () {
		// ホーム画面表示
        Route::get('home', function() {
			return view('general.home.main');
		})->name('home');

		// マイファイル一覧画面表示
		Route::get('my_files', function() {
			return view('general.my_files.main');
		})->name('my_files');

		// マイアカウント画面表示
		Route::get('my_account', function() {
			return view('general.my_account.main');
		})->name('my_account');
	});

	// 管理者ユーザーのみ遷移可能URL
	Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'can:admin_user']], function () {
		// お知らせ一覧画面表示
		Route::get('noticies', function() {
			return view('admin.noticies.main');
		})->name('noticies');

		// ユーザー一覧画面表示
		Route::get('users', function() {
			return view('admin.users.main');
		})->name('users');
	});
	
	// 全ユーザー接続可能API
	Route::group(['prefix' => 'api/general', 'middleware' => ['auth', 'can:all_user']], function(){
		// お知らせリスト取得API
		Route::get('noticies', 'General\Notice\GetNoticiesController@api');

		// マイファイルリスト取得API
		Route::get('my_files', 'General\MyFile\GetMyFilesController@api');

		// マイファイル新規登録API
		Route::post('add_my_file', 'General\MyFile\AddMyFileController@api');

		// 作業ファイル1編集API
		Route::post('edit_first_work_file', 'General\MyFile\EditFirstWorkFileController@api');

		// 作業ファイル2編集API
		Route::post('edit_second_work_file', 'General\MyFile\EditSecondWorkFileController@api');

		// 作業ファイル3編集API
		Route::post('edit_third_work_file', 'General\MyFile\EditThirdWorkFileController@api');

		// 作業日編集API
		Route::post('edit_work_date', 'General\MyFile\EditWorkDateController@api');

		// 作業メモ編集API
		Route::post('edit_work_memo', 'General\MyFile\EditWorkMemoController@api');

		// 作業場所編集API
		Route::post('edit_work_place', 'General\MyFile\EditWorkPlaceController@api');

		// マイファイル削除API
		Route::post('delete_my_file', 'General\MyFile\DeleteMyFileController@api');

		// マイアカウント取得API
		Route::get('my_account', 'General\MyAccount\GetMyAccountController@api');

		// マイアカウント名編集API
		Route::post('edit_my_account_name', 'General\MyAccount\EditMyAccountNameController@api');

		// マイアカウントメールアドレス編集API
		Route::post('edit_my_account_email', 'General\MyAccount\EditMyAccountEmailController@api');
	});

	// 管理者ユーザーのみ接続可能URL
	Route::group(['prefix' => 'api/admin', 'middleware' => ['auth', 'can:admin_user']], function () {
		// お知らせ一覧取得API
		Route::get('noticies', 'Admin\Notice\GetNoticiesController@api');

		// お知らせ新規登録API
		Route::post('add_notice', 'Admin\Notice\AddNoticeController@api');

		// お知らせ本文編集API
		Route::post('edit_notice_body', 'Admin\Notice\EditNoticeBodyController@api');

		// お知らせ削除API
		Route::post('delete_notice', 'Admin\Notice\DeleteNoticeController@api');

		// ユーザー一覧取得API
		Route::get('users', 'Admin\User\GetUsersController@api');

		// ユーザー権限リスト取得API
		Route::get('user_roles', 'Admin\User\GetUserRolesController@api');

		// ユーザー新規登録API
		Route::post('add_user', 'Admin\User\AddUserController@api');

		// ユーザー名編集API
		Route::post('edit_user_name', 'Admin\User\EditUserNameController@api');

		// ユーザーメールアドレス編集API
		Route::post('edit_user_email', 'Admin\User\EditUserEmailController@api');

		// ユーザー権限編集API
		Route::post('edit_user_role', 'Admin\User\EditUserRoleController@api');

		// ユーザー削除API
		Route::post('delete_user', 'Admin\User\DeleteUserController@api');
	});
});
