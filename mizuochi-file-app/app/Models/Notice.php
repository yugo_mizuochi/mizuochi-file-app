<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
	protected $table = 'noticies';

	protected $fillable = [
		'body'
	];

	protected $dates = [
		'created_at',
		'updated_at'
	];
}