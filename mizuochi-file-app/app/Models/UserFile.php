<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class UserFile extends Model
{
    protected $table = 'user_files';

    protected $fillable = [
        'user_id',
        'memo',
        'first_file_path',
        'second_file_path',
        'third_file_path',
        'work_date',
        'work_place'
    ];

    protected $dates = [
		'created_at',
		'updated_at'
    ];
    
    /**
     * ユーザーテーブルと紐付け
     *
     * @return $this
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}