<?php
namespace App\Entities\Common\Notice;

use App\Models\Notice as NoticeModel;

final class Notice
{
    /** @var int $id */
    private $id;

    /** @var string $body */
    private $body;

    /** @var string $createdDate */
    private $createdDate;

    /**
     * @param integer $id
     * @param string $body
     * @param string $createdDate
     */
    private function __construct(
        int $id,
        string $body,
        string $createdDate
    )
    {
        $this->id = $id;
        $this->body = $body;
        $this->createdDate = $createdDate;
    }

    /**
     * お知らせID取得
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * お知らせ本文取得
     *
     * @return string
     */
    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * お知らせ作成日取得
     *
     * @return string
     */
    public function getCreatedDate(): string
    {
        return $this->createdDate;
    }

    /**
     * お知らせEntityセット
     *
     * @param NoticeModel $noticeModel
     * @return Notice
     */
    public static function getInstance(NoticeModel $noticeModel): Notice
    {
        return new Notice(
            (int) $noticeModel->id,
            $noticeModel->body,
            $noticeModel->created_at->format('Y/m/d')
        );
    }
}