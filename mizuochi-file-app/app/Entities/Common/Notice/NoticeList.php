<?php
namespace App\Entities\Common\Notice;

use App\Entities\Common\Notice\Notice;

final class NoticeList implements \IteratorAggregate
{
    /** @var ArrayObject $noticeList */
    private $noticeList;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->noticeList = new \ArrayObject();
    }

    /**
     * お知らせリスト生成処理
     *
     * @param Notice $notice
     */
    public function add(Notice $notice)
    {
        $this->noticeList[] = $notice;
    }

    /**
     * お知らせリスト取得
     *
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return $this->noticeList->getIterator();
    }
}