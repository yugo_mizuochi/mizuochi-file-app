<?php
namespace App\Entities\Admin\Users;

use App\Models\User as UserModel;

final class User
{
    /** @var int $id */
    private $id;

    /** @var string $name */
    private $name;

    /** @var string $email */
    private $email;

    /** @var string $role */
    private $role;

    /**
     * @param integer $id
     * @param string $name
     * @param string $email
     * @param string $role
     */
    public function __construct(
        int $id,
        string $name,
        string $email,
        string $role
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->role = $role;
    }

    /**
     * ユーザーID取得
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * ユーザー名取得
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * ユーザーメールアドレス取得
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * ユーザー権限取得
     *
     * @return string
     */
    public function getRole(): string
    {
        switch ($this->role) {
            case 'general':
                return '一般';
                break;
            
            case 'admin':
                return '管理者';
                break;
        }
    }

    /**
     * ユーザーEntityセット
     *
     * @param UserModel $userModel
     * @return User
     */
    public static function getInstace(UserModel $userModel): User
    {
        return new User(
            (int) $userModel->id,
            $userModel->name,
            $userModel->email,
            $userModel->role
        );
    }
}