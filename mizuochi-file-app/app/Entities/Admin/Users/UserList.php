<?php
namespace App\Entities\Admin\Users;

use App\Entities\Admin\Users\User;

final class UserList implements \IteratorAggregate
{
    /** @var ArrayObject $userList */
    private $userList;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->userList = new \ArrayObject();
    }

    /**
     * ユーザーリスト生成
     *
     * @param User $user
     */
    public function add(User $user)
    {
        $this->userList[] = $user;
    }

    /**
     * ユーザーリスト取得
     *
     * @return ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return $this->userList->getIterator();
    }
}