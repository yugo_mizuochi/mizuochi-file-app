<?php
namespace App\Entities\General\MyAccount;

use App\Models\User as UserModel;

final class MyAccount
{
    /** @var int $id */
    private $id;

    /** @var string $name */
    private $name;

    /** @var string $email */
    private $email;

    /** @var array $todayWorkPlaces */
    private $todayWorkPlaces;

    /**
     * @param integer $id
     * @param string $name
     * @param string $email
     * @param array $todayWorkPlaces
     */
    public function __construct(
        int $id,
        string $name,
        string $email,
        array $todayWorkPlaces
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->todayWorkPlaces = $todayWorkPlaces;
    }

    /**
     * マイアカウントID取得
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * マイアカウント名取得
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * マイアカウントメールアドレス取得
     *
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * 本日の作業場所取得
     *
     * @return array
     */
    public function getTodayWorkPlaces(): array
    {
        return $this->todayWorkPlaces;
    }

    /**
     * マイアカウントEntityセット
     *
     * @param UserModel $userModel
     * @param array $todayWorkPlaces
     * @return MyAccount
     */
    public static function getInstace(UserModel $userModel, array $todayWorkPlaces): MyAccount
    {
        return new MyAccount(
            (int) $userModel->id,
            $userModel->name,
            $userModel->email,
            $todayWorkPlaces
        );
    }
}