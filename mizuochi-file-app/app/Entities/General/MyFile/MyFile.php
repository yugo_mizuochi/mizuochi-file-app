<?php
namespace App\Entities\General\MyFile;

use App\Models\UserFile as UserFileModel;

final class MyFile
{
    /** @var int $id */
    private $id;

    /** @var string $memo */
    private $memo;

    /** @var string $firstFilePath */
    private $firstFilePath;

    /** @var string $secondFilePath */
    private $secondFilePath;

    /** @var string $thirdFilePath */
    private $thirdFilePath;

    /** @var string $workDate */
    private $workDate;

    /** @var string $workPlace */
    private $workPlace;

    /**
     * @param integer $id
     * @param string $memo
     * @param string $firstFilePath
     * @param string $secondFilePath
     * @param string $thirdFilePath
     * @param string $workDate
     * @param string $workPlace
     */
    private function __construct(
        int $id,
        string $memo,
        string $firstFilePath,
        string $secondFilePath = null,
        string $thirdFilePath = null,
        string $workDate,
        string $workPlace
    )
    {
        $this->id = $id;
        $this->memo = $memo;
        $this->firstFilePath = $firstFilePath;
        $this->secondFilePath = $secondFilePath;
        $this->thirdFilePath = $thirdFilePath;
        $this->workDate = $workDate;
        $this->workPlace = $workPlace;
    }

    /**
     * ユーザーファイル情報ID取得
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * ユーザーファイルメモ取得
     *
     * @return string
     */
    public function getMemo(): string
    {
        return $this->memo;
    }

    /**
     * ユーザーファイルパス1取得
     *
     * @return string
     */
    public function getFirstFilePath(): string
    {
        return $this->firstFilePath;
    }

    /**
     * ユーザーファイルパス2取得
     *
     * @return string
     */
    public function getSecondFilePath(): string
    {
        return is_null($this->secondFilePath) ? '' : $this->secondFilePath;
    }

    /**
     * ユーザーファイルパス3取得
     *
     * @return string
     */
    public function getThirdFilePath(): string
    {
        return is_null($this->thirdFilePath) ? '' : $this->thirdFilePath;
    }

    /**
     * 作業日取得
     *
     * @return string
     */
    public function getWorkDate(): string
    {
        return $this->workDate;
    }

    /**
     * 作業場取得
     *
     * @return string
     */
    public function getWorkPlace(): string
    {
        return $this->workPlace;
    }

    /**
     * ユーザーファイルEntityセット
     *
     * @param UserFileModel $userFileModel
     * @return MyFile
     */
    public static function getInstance(UserFileModel $userFileModel): MyFile
    {
        return new MyFile(
            (int) $userFileModel->id,
            $userFileModel->memo,
            $userFileModel->first_file_path,
            $userFileModel->second_file_path,
            $userFileModel->third_file_path,
            $userFileModel->work_date,
            $userFileModel->work_place
        );
    }
}