<?php
namespace App\Entities\General\MyFile;

use App\Entities\General\MyFile\MyFile;

final class MyFileList implements \IteratorAggregate
{
    /** @var ArrayObject $myFileList */
    private $myFileList;

    /**
     * ArrayObject生成
     */
    public function __construct()
    {
        $this->myFileList = new \ArrayObject();
    }

    /**
     * ユーザーファイルリスト生成
     *
     * @param MyFile $myFile
     */
    public function add(MyFile $myFile)
    {
        $this->myFileList[] = $myFile;
    }

    /**
     * ユーザーファイルリスト取得
     *
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return $this->myFileList->getIterator();
    }
}