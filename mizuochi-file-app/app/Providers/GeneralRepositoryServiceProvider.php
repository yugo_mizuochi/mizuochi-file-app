<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GeneralRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // 一般ユーザーお知らせリポジトリパターン
        $this->app->bind(
            \App\Repositories\General\Notice\GetNoticiesRepositoryInterface::class,
            \App\Repositories\General\Notice\GetNoticiesRepository::class
        );

        // ユーザーファイルリポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\GetMyFilesRepositoryInterface::class,
            \App\Repositories\General\MyFile\GetMyFilesRepository::class
        );

        // マイファイル新規登録リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\AddMyFileRepositoryInterface::class,
            \App\Repositories\General\MyFile\AddMyFileRepository::class
        );

        // 作業ファイル1更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\EditFirstWorkFileRepositoryInterface::class,
            \App\Repositories\General\MyFile\EditFirstWorkFileRepository::class
        );

        // 作業ファイル2更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\EditSecondWorkFileRepositoryInterface::class,
            \App\Repositories\General\MyFile\EditSecondWorkFileRepository::class
        );

        // 作業ファイル3更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\EditThirdWorkFileRepositoryInterface::class,
            \App\Repositories\General\MyFile\EditThirdWorkFileRepository::class
        );

        // 作業日更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\EditWorkDateRepositoryInterface::class,
            \App\Repositories\General\MyFile\EditWorkDateRepository::class
        );

        // 作業メモ更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\EditWorkMemoRepositoryInterface::class,
            \App\Repositories\General\MyFile\EditWorkMemoRepository::class
        );

        // 作業場所更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\EditWorkPlaceRepositoryInterface::class,
            \App\Repositories\General\MyFile\EditWorkPlaceRepository::class
        );

        // マイファイル削除リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyFile\DeleteMyFileRepositoryInterface::class,
            \App\Repositories\General\MyFile\DeleteMyFileRepository::class
        );

        // マイアカウント取得リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyAccount\GetMyAccountRepositoryInterface::class,
            \App\Repositories\General\MyAccount\GetMyAccountRepository::class
        );

        // マイアカウント名更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyAccount\EditMyAccountNameRepositoryInterface::class,
            \App\Repositories\General\MyAccount\EditMyAccountNameRepository::class
        );

        // マイアカウントメールアドレス更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\General\MyAccount\EditMyAccountEmailRepositoryInterface::class,
            \App\Repositories\General\MyAccount\EditMyAccountEmailRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
