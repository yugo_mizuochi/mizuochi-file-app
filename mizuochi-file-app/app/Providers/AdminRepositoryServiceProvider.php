<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AdminRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // 管理者お知らせ一覧リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\Notice\GetNoticiesRepositoryInterface::class,
            \App\Repositories\Admin\Notice\GetNoticiesRepository::class
        );

        // お知らせ新規登録リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\Notice\AddNoticeRepositoryInterface::class,
            \App\Repositories\Admin\Notice\AddNoticeRepository::class
        );

        // お知らせ本文更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\Notice\EditNoticeBodyRepositoryInterface::class,
            \App\Repositories\Admin\Notice\EditNoticeBodyRepository::class
        );

        // お知らせ削除リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\Notice\DeleteNoticeRepositoryInterface::class,
            \App\Repositories\Admin\Notice\DeleteNoticeRepository::class
        );

        // ユーザー一覧リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\GetUsersRepositoryInterface::class,
            \App\Repositories\Admin\User\GetUsersRepository::class
        );

        // ユーザー権限リストリポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\GetUserRolesRepositoryInterface::class,
            \App\Repositories\Admin\User\GetUserRolesRepository::class
        );

        // ユーザー新規登録リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\AddUserRepositoryInterface::class,
            \App\Repositories\Admin\User\AddUserRepository::class
        );

        // ユーザー名更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\EditUserNameRepositoryInterface::class,
            \App\Repositories\Admin\User\EditUserNameRepository::class
        );

        // ユーザーメールアドレス更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\EditUserEmailRepositoryInterface::class,
            \App\Repositories\Admin\User\EditUserEmailRepository::class
        );

        // ユーザー権限更新リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\EditUserRoleRepositoryInterface::class,
            \App\Repositories\Admin\User\EditUserRoleRepository::class
        );

        // ユーザー削除リポジトリパターン
        $this->app->bind(
            \App\Repositories\Admin\User\DeleteUserRepositoryInterface::class,
            \App\Repositories\Admin\User\DeleteUserRepository::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
