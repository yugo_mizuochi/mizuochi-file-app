<?php
namespace App\Http\Controllers\Admin\Notice;

use App\UseCases\Admin\Notice\AddNoticeUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AddNoticeController extends Controller
{
    /** @var AddNoticeUseCase $addNoticeUseCase */
    private $addNoticeUseCase;

    /**
     * @param AddNoticeUseCase $addNoticeUseCase
     */
    public function __construct(AddNoticeUseCase $addNoticeUseCase)
    {
        $this->addNoticeUseCase = $addNoticeUseCase;
    }

    /**
     * お知らせ新規登録API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->addNoticeUseCase->execute($request);
    }
}