<?php
namespace App\Http\Controllers\Admin\Notice;

use App\UseCases\Admin\Notice\DeleteNoticeUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeleteNoticeController extends Controller
{
    /** @var DeleteNoticeUseCase $deleteNoticeUseCase */
    private $deleteNoticeUseCase;

    /**
     * @param DeleteNoticeUseCase $deleteNoticeUseCase
     */
    public function __construct(DeleteNoticeUseCase $deleteNoticeUseCase)
    {
        $this->deleteNoticeUseCase = $deleteNoticeUseCase;
    }

    /**
     * お知らせ削除API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->deleteNoticeUseCase->execute($request);
    }
}