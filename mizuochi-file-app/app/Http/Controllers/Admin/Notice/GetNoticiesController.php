<?php
namespace App\Http\Controllers\Admin\Notice;

use App\UseCases\Admin\Notice\GetNoticiesUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetNoticiesController extends Controller
{
    /** @var GetNoticiesUseCase $getNoticiesUseCase */
    private $getNoticiesUseCase;

    /**
     * @param GetNoticiesUseCase $getNoticiesUseCase
     */
    public function __construct(GetNoticiesUseCase $getNoticiesUseCase)
    {
        $this->getNoticiesUseCase = $getNoticiesUseCase;
    }

    /**
     * お知らせリスト取得API
     *
     * @return JsonResponse
     */
    public function api(): JsonResponse
    {
        $data = $this->getNoticiesUseCase->execute();

        return response()->json($data);
    }
}