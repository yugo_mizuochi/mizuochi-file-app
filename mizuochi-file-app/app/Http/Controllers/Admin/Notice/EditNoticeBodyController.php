<?php
namespace App\Http\Controllers\Admin\Notice;

use App\UseCases\Admin\Notice\EditNoticeBodyUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditNoticeBodyController extends Controller
{
    /** @var EditNoticeBodyUseCase $editNoticeBodyUseCase */
    private $editNoticeBodyUseCase;

    /**
     * @param EditNoticeBodyUseCase $editNoticeBodyUseCase
     */
    public function __construct(EditNoticeBodyUseCase $editNoticeBodyUseCase)
    {
        $this->editNoticeBodyUseCase = $editNoticeBodyUseCase;
    }

    /**
     * お知らせ本文編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editNoticeBodyUseCase->execute($request);
    }
}