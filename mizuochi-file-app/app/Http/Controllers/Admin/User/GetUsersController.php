<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\GetUsersUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetUsersController extends Controller
{
    /** @var GetUsersUseCase $getUsersUseCase */
    private $getUsersUseCase;

    /**
     * @param GetUsersUseCase $getUsersUseCase
     */
    public function __construct(GetUsersUseCase $getUsersUseCase)
    {
        $this->getUsersUseCase = $getUsersUseCase;
    }

    /**
     * ユーザーリスト取得API
     *
     * @return JsonResponse
     */
    public function api(): JsonResponse
    {
        $data = $this->getUsersUseCase->execute();

        return response()->json($data);
    }
}