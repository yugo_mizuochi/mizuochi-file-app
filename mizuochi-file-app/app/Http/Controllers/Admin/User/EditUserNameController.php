<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\EditUserNameUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditUserNameController extends Controller
{
    /** @var EditUserNameUseCase $editUserNameUseCase */
    private $editUserNameUseCase;

    /**
     * @param EditUserNameUseCase $editUserNameUseCase
     */
    public function __construct(EditUserNameUseCase $editUserNameUseCase)
    {
        $this->editUserNameUseCase = $editUserNameUseCase;
    }

    /**
     * ユーザー名編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editUserNameUseCase->execute($request);
    }
}