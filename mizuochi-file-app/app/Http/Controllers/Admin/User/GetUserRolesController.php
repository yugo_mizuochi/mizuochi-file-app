<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\GetUserRolesUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetUserRolesController extends Controller
{
    /** @var GetUserRolesUseCase $getUserRolesUseCase */
    private $getUserRolesUseCase;

    /**
     * @param GetUserRolesUseCase $getUserRolesUseCase
     */
    public function __construct(GetUserRolesUseCase $getUserRolesUseCase)
    {
        $this->getUserRolesUseCase = $getUserRolesUseCase;
    }

    /**
     * ユーザー権限リスト取得API
     *
     * @return JsonResponse
     */
    public function api(): JsonResponse
    {
        $data = $this->getUserRolesUseCase->execute();

        return response()->json($data);
    }
}