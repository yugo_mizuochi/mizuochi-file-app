<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\EditUserRoleUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditUserRoleController extends Controller
{
    /** @var EditUserRoleUseCase $editUserRoleUseCase */
    private $editUserRoleUseCase;

    /**
     * @param EditUserRoleUseCase $editUserRoleUseCase
     */
    public function __construct(EditUserRoleUseCase $editUserRoleUseCase)
    {
        $this->editUserRoleUseCase = $editUserRoleUseCase;
    }

    /**
     * ユーザー権限編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editUserRoleUseCase->execute($request);
    }
}