<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\DeleteUserUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DeleteUserController extends Controller
{
    /** @var DeleteUserUseCase $deleteUserUseCase */
    private $deleteUserUseCase;

    /**
     * @param DeleteUserUseCase $deleteUserUseCase
     */
    public function __construct(DeleteUserUseCase $deleteUserUseCase)
    {
        $this->deleteUserUseCase = $deleteUserUseCase;
    }

    /**
     * ユーザー削除API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->deleteUserUseCase->execute($request);
    }
}