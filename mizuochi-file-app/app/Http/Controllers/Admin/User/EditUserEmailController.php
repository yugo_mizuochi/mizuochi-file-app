<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\EditUserEmailUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditUserEmailController extends Controller
{
    /** @var EditUserEmailUseCase $editUserEmailUseCase */
    private $editUserEmailUseCase;

    /**
     * @param EditUserEmailUseCase $editUserEmailUseCase
     */
    public function __construct(EditUserEmailUseCase $editUserEmailUseCase)
    {
        $this->editUserEmailUseCase = $editUserEmailUseCase;
    }

    /**
     * ユーザーメールアドレス編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editUserEmailUseCase->execute($request);
    }
}