<?php
namespace App\Http\Controllers\Admin\User;

use App\UseCases\Admin\User\AddUserUseCase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddUserController extends Controller
{
    /** @var AddUserUseCase $addUserUseCase */
    private $addUserUseCase;

    /**
     * @param AddUserUseCase $addUserUseCase
     */
    public function __construct(AddUserUseCase $addUserUseCase)
    {
        $this->addUserUseCase = $addUserUseCase;
    }

    /**
     * ユーザー新規登録API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->addUserUseCase->execute($request);
    }
}