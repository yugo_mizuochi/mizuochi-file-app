<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\AddMyFileUseCase;
use Illuminate\Http\Request;

class AddMyFileController extends Controller
{
    /** @var AddMyFileUseCase $addMyFileUseCase */
    private $addMyFileUseCase;

    /**
     * @param AddMyFileUseCase $addMyFileUseCase
     */
    public function __construct(AddMyFileUseCase $addMyFileUseCase)
    {
        $this->addMyFileUseCase = $addMyFileUseCase;
    }

    /**
     * マイファイル新規登録API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->addMyFileUseCase->execute($request);
    }
}