<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\EditSecondWorkFileUseCase;
use Illuminate\Http\Request;

class EditSecondWorkFileController extends Controller
{
    /** @var EditSecondWorkFileUseCase $editSecondWorkFileUseCase */
    private $editSecondWorkFileUseCase;

    /**
     * @param EditSecondWorkFileUseCase $editSecondWorkFileUseCase
     */
    public function __construct(EditSecondWorkFileUseCase $editSecondWorkFileUseCase)
    {
        $this->editSecondWorkFileUseCase = $editSecondWorkFileUseCase;
    }

    /**
     * 作業ファイル2編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editSecondWorkFileUseCase->execute($request);
    }
}