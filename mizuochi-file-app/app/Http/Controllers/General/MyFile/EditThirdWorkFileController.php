<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\EditThirdWorkFileUseCase;
use Illuminate\Http\Request;

class EditThirdWorkFileController extends Controller
{
    /** @var EditThirdWorkFileUseCase $editThirdWorkFileUseCase */
    private $editThirdWorkFileUseCase;

    /**
     * @param EditThirdWorkFileUseCase $editThirdWorkFileUseCase
     */
    public function __construct(EditThirdWorkFileUseCase $editThirdWorkFileUseCase)
    {
        $this->editThirdWorkFileUseCase = $editThirdWorkFileUseCase;
    }

    /**
     * 作業ファイル3編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editThirdWorkFileUseCase->execute($request);
    }
}