<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\EditWorkDateUseCase;
use Illuminate\Http\Request;

class EditWorkDateController extends Controller
{
    /** @var EditWorkDateUseCase $editWorkDateUseCase */
    private $editWorkDateUseCase;

    /**
     * @param EditWorkDateUseCase $editWorkDateUseCase
     */
    public function __construct(EditWorkDateUseCase $editWorkDateUseCase)
    {
        $this->editWorkDateUseCase = $editWorkDateUseCase;
    }

    /**
     * 作業日編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editWorkDateUseCase->execute($request);
    }
}