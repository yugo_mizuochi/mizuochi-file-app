<?php
namespace App\Http\Controllers\General\MyFile;

use App\UseCases\General\MyFile\GetMyFilesUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class GetMyFilesController extends Controller
{
    /** @var GetMyFilesUseCase $getMyFilesUseCase */
    private $getMyFilesUseCase;

    /**
     * @param GetMyFilesUseCase $getMyFilesUseCase
     */
    public function __construct(GetMyFilesUseCase $getMyFilesUseCase)
    {
        $this->getMyFilesUseCase = $getMyFilesUseCase;
    }

    /**
     * マイファイルリスト取得API
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function api(Request $request): JsonResponse
    {
        $data = $this->getMyFilesUseCase->execute($request);

        return response()->json($data);
    }
}