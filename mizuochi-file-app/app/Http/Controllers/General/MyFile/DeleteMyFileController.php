<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\DeleteMyFileUseCase;
use Illuminate\Http\Request;

class DeleteMyFileController extends Controller
{
    /** @var DeleteMyFileUseCase $deleteMyFileUseCase */
    private $deleteMyFileUseCase;

    /**
     * @param DeleteMyFileUseCase $deleteMyFileUseCase
     */
    public function __construct(DeleteMyFileUseCase $deleteMyFileUseCase)
    {
        $this->deleteMyFileUseCase = $deleteMyFileUseCase;
    }

    /**
     * マイファイル削除API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->deleteMyFileUseCase->execute($request);
    }
}