<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\EditFirstWorkFileUseCase;
use Illuminate\Http\Request;

class EditFirstWorkFileController extends Controller
{
    /** @var EditFirstWorkFileUseCase $editFirstWorkFileUseCase */
    private $editFirstWorkFileUseCase;

    /**
     * @param EditFirstWorkFileUseCase $editFirstWorkFileUseCase
     */
    public function __construct(EditFirstWorkFileUseCase $editFirstWorkFileUseCase)
    {
        $this->editFirstWorkFileUseCase = $editFirstWorkFileUseCase;
    }

    /**
     * 作業ファイル1編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editFirstWorkFileUseCase->execute($request);
    }
}