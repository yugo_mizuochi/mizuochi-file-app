<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\EditWorkPlaceUseCase;
use Illuminate\Http\Request;

class EditWorkPlaceController extends Controller
{
    /** @var EditWorkPlaceUseCase $editWorkPlaceUseCase */
    private $editWorkPlaceUseCase;

    /**
     * @param EditWorkPlaceUseCase $editWorkPlaceUseCase
     */
    public function __construct(EditWorkPlaceUseCase $editWorkPlaceUseCase)
    {
        $this->editWorkPlaceUseCase = $editWorkPlaceUseCase;
    }

    /**
     * 作業場所編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editWorkPlaceUseCase->execute($request);
    }
}