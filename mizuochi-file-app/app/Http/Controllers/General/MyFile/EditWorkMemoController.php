<?php
namespace App\Http\Controllers\General\MyFile;

use App\Http\Controllers\Controller;
use App\UseCases\General\MyFile\EditWorkMemoUseCase;
use Illuminate\Http\Request;

class EditWorkMemoController extends Controller
{
    /** @var EditWorkMemoUseCase $editWorkMemoUseCase */
    private $editWorkMemoUseCase;

    /**
     * @param EditWorkMemoUseCase $editWorkMemoUseCase
     */
    public function __construct(EditWorkMemoUseCase $editWorkMemoUseCase)
    {
        $this->editWorkMemoUseCase = $editWorkMemoUseCase;
    }

    /**
     * 作業ファイル編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editWorkMemoUseCase->execute($request);
    }
}