<?php
namespace App\Http\Controllers\General\MyAccount;

use App\UseCases\General\MyAccount\EditMyAccountEmailUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditMyAccountEmailController extends Controller
{
    /** @var EditMyAccountEmailUseCase $editMyAccountEmailUseCase */
    private $editMyAccountEmailUseCase;

    /**
     * @param EditMyAccountEmailUseCase $editMyAccountEmailUseCase
     */
    public function __construct(EditMyAccountEmailUseCase $editMyAccountEmailUseCase)
    {
        $this->editMyAccountEmailUseCase = $editMyAccountEmailUseCase;
    }

    /**
     * マイアカウントメールアドレス編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editMyAccountEmailUseCase->execute($request);
    }
}