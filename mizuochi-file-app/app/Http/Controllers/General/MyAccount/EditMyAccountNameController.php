<?php
namespace App\Http\Controllers\General\MyAccount;

use App\UseCases\General\MyAccount\EditMyAccountNameUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditMyAccountNameController extends Controller
{
    /** @var EditMyAccountNameUseCase $editMyAccountNameUseCase */
    private $editMyAccountNameUseCase;

    /**
     * @param EditMyAccountNameUseCase $editMyAccountNameUseCase
     */
    public function __construct(EditMyAccountNameUseCase $editMyAccountNameUseCase)
    {
        $this->editMyAccountNameUseCase = $editMyAccountNameUseCase;
    }

    /**
     * マイアカウント名編集API
     *
     * @param Request $request
     */
    public function api(Request $request)
    {
        $this->editMyAccountNameUseCase->execute($request);
    }
} 