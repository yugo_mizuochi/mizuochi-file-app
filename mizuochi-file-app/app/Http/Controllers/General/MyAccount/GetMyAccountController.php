<?php
namespace App\Http\Controllers\General\MyAccount;

use App\UseCases\General\MyAccount\GetMyAccountUseCase;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

class GetMyAccountController extends Controller
{
    /** @var GetMyAccountUseCase */
    private $getMyAccountUseCase;

    /**
     * @param GetMyAccountUseCase $getMyAccountUseCase
     */
    public function __construct(GetMyAccountUseCase $getMyAccountUseCase)
    {
        $this->getMyAccountUseCase = $getMyAccountUseCase;
    }

    /**
     * マイアカウント取得API
     *
     * @return JsonResponse
     */
    public function api(): JsonResponse
    {
        $data = $this->getMyAccountUseCase->execute();

        return response()->json($data);
    }
}