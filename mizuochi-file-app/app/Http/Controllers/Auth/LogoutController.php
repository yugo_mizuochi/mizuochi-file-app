<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class LogoutController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
     * ログアウト処理
     *
     * @return redirect
     */
	public function logout()
	{
		Auth::logout();
		return redirect('/');
	}
}