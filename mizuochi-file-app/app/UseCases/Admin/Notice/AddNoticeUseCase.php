<?php
namespace App\UseCases\Admin\Notice;

use App\Repositories\Admin\Notice\AddNoticeRepositoryInterface as AddNoticeRepository;
use Illuminate\Http\Request;

final class AddNoticeUseCase
{
    /** @var AddNoticeRepository $addNoticeRepository */
    private $addNoticeRepository;

    public function __construct(AddNoticeRepository $addNoticeRepository)
    {
        $this->addNoticeRepository = $addNoticeRepository;
    }

    /**
     * Repositoryにお知らせ登録を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $addNoticeParams = [
            'notice_body' => $request->input('notice_body')
        ];

        $this->addNoticeRepository->execute($addNoticeParams);
    }
}