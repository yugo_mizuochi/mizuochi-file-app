<?php
namespace App\UseCases\Admin\Notice;

use App\Repositories\Admin\Notice\DeleteNoticeRepositoryInterface as DeleteNoticeRepository;
use Illuminate\Http\Request;

final class DeleteNoticeUseCase
{
    /** @var DeleteNoticeRepository $deleteNoticeRepository */
    private $deleteNoticeRepository;

    /**
     * @param DeleteNoticeRepository $deleteNoticeRepository
     */
    public function __construct(DeleteNoticeRepository $deleteNoticeRepository)
    {
        $this->deleteNoticeRepository = $deleteNoticeRepository;
    }

    /**
     * Repositoryにお知らせ削除を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $deleteNoticeParams = [
            'delete_notice_id' => $request->input('delete_notice_id')
        ];

        $this->deleteNoticeRepository->execute($deleteNoticeParams);
    }
}