<?php
namespace App\UseCases\Admin\Notice;

use App\Repositories\Admin\Notice\EditNoticeBodyRepositoryInterface as EditNoticeBodyRepository;
use Illuminate\Http\Request;

final class EditNoticeBodyUseCase
{
    /** @var EditNoticeBodyRepository $editNoticeBodyRepository */
    private $editNoticeBodyRepository;

    /**
     * @param EditNoticeBodyRepository $editNoticeBodyRepository
     */
    public function __construct(EditNoticeBodyRepository $editNoticeBodyRepository)
    {
        $this->editNoticeBodyRepository = $editNoticeBodyRepository;
    }

    /**
     * Repositoryにお知らせ本文の編集を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editNoticeBodyParams = [
            'notice_id' => $request->input('notice_id'),
            'change_notice_body' => $request->input('change_notice_body')
        ];

        $this->editNoticeBodyRepository->execute($editNoticeBodyParams);
    }
}