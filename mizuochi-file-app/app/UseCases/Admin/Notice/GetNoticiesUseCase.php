<?php
namespace App\UseCases\Admin\Notice;

use App\Repositories\Admin\Notice\GetNoticiesRepositoryInterface as GetNoticiesRepository;
use App\Entities\Common\Notice\NoticeList;

final class GetNoticiesUseCase
{
    /** @var GetNoticiesRepository $getNoticiesRepository */
    private $getNoticiesRepository;

    /**
     * @param GetNoticiesRepository $getNoticiesRepository
     */
    public function __construct(GetNoticiesRepository $getNoticiesRepository)
    {
        $this->getNoticiesRepository = $getNoticiesRepository;
    }

    /**
     * お知らせリストをRepositoryから取得
     *
     * @return array
     */
    public function execute(): array
    {
        $noticies = $this->getNoticiesRepository->execute();

        return $this->conversion($noticies);
    }

    /**
     * お知らせリストを整形
     *
     * @param NoticeList $noticies
     * @return array
     */
    private static function conversion(NoticeList $noticies): array
    {
        $data = [];
        foreach ($noticies as $notice) {
            $data[] = [
                'id' => $notice->getId(),
                'body' => $notice->getBody()
            ];
        }
        return $data;
    }
}