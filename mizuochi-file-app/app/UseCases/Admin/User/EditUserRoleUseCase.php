<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\EditUserRoleRepositoryInterface as EditUserRoleRepository;
use Illuminate\Http\Request;

final class EditUserRoleUseCase
{
    /** @var EditUserRoleRepository $editUserRoleRepository */
    private $editUserRoleRepository;

    /**
     * @param EditUserRoleRepository $editUserRoleRepository
     */
    public function __construct(EditUserRoleRepository $editUserRoleRepository)
    {
        $this->editUserRoleRepository = $editUserRoleRepository;
    }

    /**
     * Repositoryにユーザー権限編集を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editUserRoleParams = [
            'user_id' => $request->input('user_id'),
            'change_user_role' => $request->input('change_user_role')
        ];

        $this->editUserRoleRepository->execute($editUserRoleParams);
    }
}