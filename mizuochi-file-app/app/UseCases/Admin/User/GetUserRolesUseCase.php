<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\GetUserRolesRepositoryInterface as GetUserRolesRepository;

final class GetUserRolesUseCase
{
    /** @var GetUserRolesRepository $getUserRolesRepository */
    private $getUserRolesRepository;

    /**
     * @param GetUserRolesRepository $getUserRolesRepository
     */
    public function __construct(GetUserRolesRepository $getUserRolesRepository)
    {
        $this->getUserRolesRepository = $getUserRolesRepository;
    }

    /**
     * ユーザー権限リスト取得を依頼
     *
     * @return array
     */
    public function execute(): array
    {
        return $this->getUserRolesRepository->execute();
    }
}