<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\AddUserRepositoryInterface as AddUserRepository;
use Illuminate\Http\Request;

final class AddUserUseCase
{
    /** @var AddUserRepository $addUserRepository */
    private $addUserRepository;

    /**
     * @param AddUserRepository $addUserRepository
     */
    public function __construct(AddUserRepository $addUserRepository)
    {
        $this->addUserRepository = $addUserRepository;
    }

    /**
     * Repositoryにユーザー新規登録を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $addUserParams = [
            'user_name' => $request->input('user_name'),
            'user_email' => $request->input('user_email'),
            'user_password' => $request->input('user_password'),
            'user_role' => $request->input('user_role')
        ];

        $this->addUserRepository->execute($addUserParams);
    }
}