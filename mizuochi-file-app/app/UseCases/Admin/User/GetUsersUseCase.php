<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\GetUsersRepositoryInterface as GetUsersRepository;
use App\Entities\Admin\Users\UserList;

final class GetUsersUseCase
{
    /** @var GetUsersRepository $getUsersRepository */
    private $getUsersRepository;

    /**
     * @param GetUsersRepository $getUsersRepository
     */
    public function __construct(GetUsersRepository $getUsersRepository)
    {
        $this->getUsersRepository = $getUsersRepository;
    }

    /**
     * ユーザーリストをRepositoryから取得
     *
     * @return array
     */
    public function execute(): array
    {
        $users = $this->getUsersRepository->execute();

        return self::conversion($users);
    }

    /**
     * ユーザーリストを整形
     *
     * @param UserList $users
     * @return array
     */
    private static function conversion(UserList $users): array
    {
        $data = [];
        foreach ($users as $user) {
            $data[] = [
                'id' => $user->getId(),
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'role' => $user->getRole()
            ];
        }
        return $data;
    }
}