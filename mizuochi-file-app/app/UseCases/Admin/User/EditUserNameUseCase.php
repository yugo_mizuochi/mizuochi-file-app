<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\EditUserNameRepositoryInterface as EditUserNameRepository;
use Illuminate\Http\Request;

final class EditUserNameUseCase
{
    /** @var EditUserNameRepository $editUserNameRepository */
    private $editUserNameRepository;

    /**
     * @param EditUserNameRepository $editUserNameRepository
     */
    public function __construct(EditUserNameRepository $editUserNameRepository)
    {
        $this->editUserNameRepository = $editUserNameRepository;
    }

    /**
     * Repositoryにユーザー名編集を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editUserNameParams = [
            'user_id' => $request->input('user_id'),
            'change_user_name' => $request->input('change_user_name')
        ];

        $this->editUserNameRepository->execute($editUserNameParams);
    }
}