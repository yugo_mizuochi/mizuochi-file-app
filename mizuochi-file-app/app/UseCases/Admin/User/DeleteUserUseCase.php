<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\DeleteUserRepositoryInterface as DeleteUserRepository;
use Illuminate\Http\Request;

final class DeleteUserUseCase extends UserFileBaseUseCase
{
    /** @var DeleteUserRepository $deleteUserRepository */
    private $deleteUserRepository;

    /**
     * @param DeleteUserRepository $deleteUserRepository
     */
    public function __construct(DeleteUserRepository $deleteUserRepository)
    {
        $this->deleteUserRepository = $deleteUserRepository;
    }

    /**
     * Repositoryにユーザーファイルとユーザー削除を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $userId = (int) $request->input('delete_user_id');
        $this->deleteUserRepository->execute($userId);
        $this->deleteAccountDelectory($userId);
    }
}