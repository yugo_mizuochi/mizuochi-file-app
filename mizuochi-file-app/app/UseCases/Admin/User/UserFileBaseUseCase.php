<?php
namespace App\UseCases\Admin\User;

use Illuminate\Support\Facades\Storage;

class UserFileBaseUseCase
{
    /**
     * アカウントのディレクトリごと削除
     *
     * @param integer $myId
     */
    protected function deleteAccountDelectory(int $myId)
    {
        Storage::disk('s3')->deleteDirectory($myId);
    }
}