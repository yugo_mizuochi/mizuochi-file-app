<?php
namespace App\UseCases\Admin\User;

use App\Repositories\Admin\User\EditUserEmailRepositoryInterface as EditUserEmailRepository;
use Illuminate\Http\Request;

final class EditUserEmailUseCase
{
    /** @var EditUserEmailRepository $editUserEmailRepository */
    private $editUserEmailRepository;

    /**
     * @param EditUserEmailRepository $editUserEmailRepository
     */
    public function __construct(EditUserEmailRepository $editUserEmailRepository)
    {
        $this->editUserEmailRepository = $editUserEmailRepository;
    }

    /**
     * Repositoryにユーザーメールアドレス編集依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editUserEmailParams = [
            'user_id' => $request->input('user_id'),
            'change_user_email' => $request->input('change_user_email')
        ];

        $this->editUserEmailRepository->execute($editUserEmailParams);
    }
}