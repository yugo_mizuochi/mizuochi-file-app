<?php
namespace App\UseCases\General\MyAccount;

use App\Entities\General\MyAccount\MyAccount;
use App\Repositories\General\MyAccount\GetMyAccountRepositoryInterface as GetMyAccountRepository;
use Illuminate\Support\Facades\Auth;

final class GetMyAccountUseCase
{
    /** @var GetMyAccountRepository $getMyAccountRepository */
    private $getMyAccountRepository;

    /**
     * @param GetMyAccountRepository $getMyAccountRepository
     */
    public function __construct(GetMyAccountRepository $getMyAccountRepository)
    {
        $this->getMyAccountRepository = $getMyAccountRepository;
    }

    /**
     * マイアカウント情報をRepositoryから取得
     *
     * @return array
     */
    public function execute(): array
    {
        $getMyAccountParams = [
            'my_id' => (int) Auth::id()
        ];

        $myAccount = $this->getMyAccountRepository->execute($getMyAccountParams);

        return self::conversion($myAccount);
    }

    /**
     * マイアカウントデータ整形
     *
     * @param MyAccount $myAccount
     * @return array
     */
    private static function conversion(MyAccount $myAccount): array
    {
        $data = [
            'id' => $myAccount->getId(),
            'name' => $myAccount->getName(),
            'email' => $myAccount->getEmail(),
            'today_work_places' => $myAccount->getTodayWorkPlaces()
        ];

        return $data;
    }
}