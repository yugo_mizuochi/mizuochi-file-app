<?php
namespace App\UseCases\General\MyAccount;

use App\Repositories\General\MyAccount\EditMyAccountEmailRepositoryInterface as EditMyAccountEmailRepository;
use Illuminate\Http\Request;

final class EditMyAccountEmailUseCase
{
    /** @var EditMyAccountEmailRepository */
    private $editMyAccountEmailRepository;

    /**
     * @param EditMyAccountEmailRepository $editMyAccountEmailRepository
     */
    public function __construct(EditMyAccountEmailRepository $editMyAccountEmailRepository)
    {
        $this->editMyAccountEmailRepository = $editMyAccountEmailRepository;
    }

    /**
     * Repositoryにマイアカウントメールアドレス編集を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editMyAccountEmailParams = [
            'my_account_id' => $request->input('my_account_id'),
            'change_my_account_email' => $request->input('change_my_account_email')
        ];

        $this->editMyAccountEmailRepository->execute($editMyAccountEmailParams);
    }
}