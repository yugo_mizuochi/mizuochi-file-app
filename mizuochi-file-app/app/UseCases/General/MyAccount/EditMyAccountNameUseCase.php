<?php
namespace App\UseCases\General\MyAccount;

use App\Repositories\General\MyAccount\EditMyAccountNameRepositoryInterface as EditMyAccountNameRepository;
use Illuminate\Http\Request;

final class EditMyAccountNameUseCase
{
    /** @var EditMyAccountNameRepository $editMyAccountNameRepository */
    private $editMyAccountNameRepository;

    /**
     * @param EditMyAccountNameRepository $editMyAccountNameRepository
     */
    public function __construct(EditMyAccountNameRepository $editMyAccountNameRepository)
    {
        $this->editMyAccountNameRepository = $editMyAccountNameRepository;
    }

    /**
     * Repositoryにマイアカウント名の編集依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editMyAccountNameParams = [
            'my_account_id' => $request->input('my_account_id'),
            'change_my_account_name' => $request->input('change_my_account_name')
        ];

        $this->editMyAccountNameRepository->execute($editMyAccountNameParams);
    }
}