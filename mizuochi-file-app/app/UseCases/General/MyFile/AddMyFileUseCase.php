<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\AddMyFileRepositoryInterface as AddMyFileRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class AddMyFileUseCase extends MyFileBaseUseCase
{
    /** @var AddMyFileRepository $addMyFileRepository */
    private $addMyFileRepository;

    public function __construct(AddMyFileRepository $addMyFileRepository)
    {
        $this->addMyFileRepository = $addMyFileRepository;
    }

    /**
     * ファイルをstorageに保存、Repositoryにmodel保存を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $firstWorkFile = $request->file('first_work_file');
        $secondWorkFile = $request->file('second_work_file');
        $thirdWorkFile = $request->file('third_work_file');
        $myId = (int) Auth::id();
        $workDate = $request->input('work_date');
        $addParams = [
            'my_id' => $myId,
            'memo' => $request->input('memo'),
            'work_date' => $workDate,
            'work_place' => $request->input('work_place')
        ];

        $firstFilePath = $this->processWorkFile($firstWorkFile, $myId, $workDate);
        $addParams['first_file_path'] = $firstFilePath;

        if (!is_null($secondWorkFile)) {
            $secondFilePath = $this->processWorkFile($secondWorkFile, $myId, $workDate);
            $addParams['second_file_path'] = $secondFilePath;
        } else {
            $addParams['second_file_path'] = null;
        }

        if (!is_null($thirdWorkFile)) {
            $thirdFilePath = $this->processWorkFile($thirdWorkFile, $myId, $workDate);
            $addParams['third_file_path'] = $thirdFilePath;
        } else {
            $addParams['third_file_path'] = null;
        }

        $this->addMyFileRepository->execute($addParams);
    }
}