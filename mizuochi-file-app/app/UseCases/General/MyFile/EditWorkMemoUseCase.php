<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\EditWorkMemoRepositoryInterface as EditWorkMemoRepository;
use Illuminate\Http\Request;

final class EditWorkMemoUseCase extends MyFileBaseUseCase
{
    /** @var EditWorkMemoRepository $editWorkMemoRepository */
    private $editWorkMemoRepository;

    /**
     * @param EditWorkMemoRepository $editWorkMemoRepository
     */
    public function __construct(EditWorkMemoRepository $editWorkMemoRepository)
    {
        $this->editWorkMemoRepository = $editWorkMemoRepository;
    }

    /**
     * 作業メモの編集をRepositoryに依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editWorkMemoParams = [
            'my_file_id' => $request->input('my_file_id'),
            'change_work_memo' => $request->input('change_work_memo')
        ];

        $this->editWorkMemoRepository->execute($editWorkMemoParams);
    }
}