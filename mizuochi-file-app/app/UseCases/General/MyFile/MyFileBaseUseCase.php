<?php
namespace App\UseCases\General\MyFile;

use Illuminate\Support\Facades\Storage;

class MyFileBaseUseCase
{
    /** @var string */
    const AWS_S3_BASE_URL = 'https://file-management-app-bucket.s3.ap-northeast-1.amazonaws.com/';

    /**
     * ファイルをストレージに保存し、model保存用のパスを返す
     *
     * @param object $workFile
     * @param integer $myId
     * @param string $workDate
     * @return string
     */
    protected function processWorkFile(
        object $workFile, 
        int $myId, 
        string $workDate
    ): string
    {
        $workFileName = $workFile->getClientOriginalName();
        $filePath = Storage::disk('s3')->putFileAs("/{$myId}/$workDate", $workFile, $workFileName, 'public');
        return Storage::disk('s3')->url($filePath);
    }

    /**
     * 現在Storageにあるファイルを削除する
     *
     * @param string $filePath
     */
    protected function deleteStorageFile(string $filePath)
    {
        $storageFilePath = str_replace(self::AWS_S3_BASE_URL, '', $filePath);
        Storage::disk('s3')->delete($storageFilePath);
    }

    /**
     * 現在のファイルを変更作業日のディレクトリに移動
     *
     * @param string $workFilePath
     * @param integer $myId
     * @param string $changeWorkDate
     * @return string
     */
    protected function changeFileDelectory(
        string $workFilePath,
        int $myId,
        string $changeWorkDate
    ): string
    {
        $disk = Storage::disk('s3');
        $workFileName = basename($workFilePath);
        $currentFilePath = str_replace(self::AWS_S3_BASE_URL, '', $workFilePath);
        $changeFilePath = "{$myId}/{$changeWorkDate}/{$workFileName}";
        $disk->move($currentFilePath, $changeFilePath);
        return $disk->url($changeFilePath);
    }
}