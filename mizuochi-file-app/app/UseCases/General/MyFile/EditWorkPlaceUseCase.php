<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\EditWorkPlaceRepositoryInterface as EditWorkPlaceRepository;
use Illuminate\Http\Request;

final class EditWorkPlaceUseCase extends MyFileBaseUseCase
{
    /** @var EditWorkPlaceRepository $editWorkPlaceRepository */
    private $editWorkPlaceRepository;

    /**
     * @param EditWorkPlaceRepository $editWorkPlaceRepository
     */
    public function __construct(EditWorkPlaceRepository $editWorkPlaceRepository)
    {
        $this->editWorkPlaceRepository = $editWorkPlaceRepository;
    }

    /**
     * 作業場所の編集をRepositoryに依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $editWorkPlaceParams = [
            'my_file_id' => $request->input('my_file_id'),
            'change_work_place' => $request->input('change_work_place')
        ];

        $this->editWorkPlaceRepository->execute($editWorkPlaceParams);
    }
}