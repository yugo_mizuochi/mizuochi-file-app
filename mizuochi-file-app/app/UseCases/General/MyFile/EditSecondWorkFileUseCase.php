<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\EditSecondWorkFileRepositoryInterface as EditSecondWorkFileRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class EditSecondWorkFileUseCase extends MyFileBaseUseCase
{
    /** @var EditSecondWorkFileRepository $editSecondWorkFileRepository */
    private $editSecondWorkFileRepository;

    /**
     * @param EditSecondWorkFileRepository $editSecondWorkFileRepository
     */
    public function __construct(EditSecondWorkFileRepository $editSecondWorkFileRepository)
    {
        $this->editSecondWorkFileRepository = $editSecondWorkFileRepository;
    }

    /**
     * 変更前のファイルをStorageから削除
     * 更新するファイルをStorageに保存
     * RepositoryにModel更新を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $changeFile = $request->file('change_file');
        $myId = (int) Auth::id();
        $workDate = $request->input('work_date');
        $secondMyFile = $request->input('second_file_path');

        if (!is_null($secondMyFile)) {
            $this->deleteStorageFile($secondMyFile);
        }

        if (!is_null($changeFile)) {
            $changeSecondWorkFilePath = $this->processWorkFile($changeFile, $myId, $workDate);
        } else {
            $changeSecondWorkFilePath = null;
        }

        $editSecondWorkFileParams = [
            'my_file_id' => $request->input('my_file_id'),
            'change_second_work_file_path' => $changeSecondWorkFilePath
        ];

        $this->editSecondWorkFileRepository->execute($editSecondWorkFileParams);
    }
}