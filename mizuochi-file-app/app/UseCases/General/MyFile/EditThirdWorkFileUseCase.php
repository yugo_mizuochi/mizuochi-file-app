<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\EditThirdWorkFileRepositoryInterface as EditThirdWorkFileRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class EditThirdWorkFileUseCase extends MyFileBaseUseCase
{
    /** @var EditThirdWorkFileRepository $editThirdWorkFileRepository */
    private $editThirdWorkFileRepository;

    /**
     * @param EditThirdWorkFileRepository $editThirdWorkFileRepository
     */
    public function __construct(EditThirdWorkFileRepository $editThirdWorkFileRepository)
    {
        $this->editThirdWorkFileRepository = $editThirdWorkFileRepository;
    }

    /**
     * 変更前のファイルをStorageから削除
     * 更新するファイルをStorageに保存
     * RepositoryにModel更新を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $changeFile = $request->file('change_file');
        $myId = (int) Auth::id();
        $workDate = $request->input('work_date');
        $thirdMyFile = $request->input('third_file_path');

        if (!is_null($thirdMyFile)) {
            $this->deleteStorageFile($thirdMyFile);
        }

        if (!is_null($changeFile)) {
            $changeThirdWorkFilePath = $this->processWorkFile($changeFile, $myId, $workDate);
        } else {
            $changeThirdWorkFilePath = null;
        }

        $editThirdWorkFileParams = [
            'my_file_id' => $request->input('my_file_id'),
            'change_third_work_file_path' => $changeThirdWorkFilePath
        ];

        $this->editThirdWorkFileRepository->execute($editThirdWorkFileParams);
    }
}