<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\EditFirstWorkFileRepositoryInterface as EditFirstWorkFileRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class EditFirstWorkFileUseCase extends MyFileBaseUseCase
{
    /** @var EditFirstWorkFileRepository $editFirstWorkFileRepository */
    private $editFirstWorkFileRepository;

    /**
     * @param EditFirstWorkFileRepository $editFirstWorkFileRepository
     */
    public function __construct(EditFirstWorkFileRepository $editFirstWorkFileRepository)
    {
        $this->editFirstWorkFileRepository = $editFirstWorkFileRepository;
    }

    /**
     * 変更前のファイルをStorageから削除
     * 更新するファイルをStorageに保存
     * RepositoryにModel更新を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $changeFile = $request->file('change_file');
        $myId = (int) Auth::id();
        $workDate = $request->input('work_date');

        $this->deleteStorageFile($request->input('first_file_path'));
        $changeFirstWorkFilePath = $this->processWorkFile($changeFile, $myId, $workDate);

        $editFirstWorkFileParams = [
            'my_file_id' => $request->input('my_file_id'),
            'change_first_work_file_path' => $changeFirstWorkFilePath
        ];

        $this->editFirstWorkFileRepository->execute($editFirstWorkFileParams);
    }
}