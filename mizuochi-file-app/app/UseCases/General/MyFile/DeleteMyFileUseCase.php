<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\DeleteMyFileRepositoryInterface as DeleteMyFileRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class DeleteMyFileUseCase extends MyFileBaseUseCase
{
    /** @var DeleteMyFileRepository $deleteMyFileRepository */
    private $deleteMyFileRepository;

    /**
     * @param DeleteMyFileRepository $deleteMyFileRepository
     */
    public function __construct(DeleteMyFileRepository $deleteMyFileRepository)
    {
        $this->deleteMyFileRepository = $deleteMyFileRepository;
    }

    /**
     * ストレージのファイルを削除し、リポジトリにマイファイル削除を依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $firstFilePath = $request->input('first_file_path');
        $secondFilePath = $request->input('second_file_path');
        $thirdFilePath = $request->input('third_file_path');
        $deleteMyFileParams = [
            'my_file_id' => $request->input('my_file_id')
        ];

        $this->deleteStorageFile($firstFilePath);

        if (!is_null($secondFilePath)) {
            $this->deleteStorageFile($secondFilePath);
        }

        if (!is_null($thirdFilePath)) {
            $this->deleteStorageFile($thirdFilePath);
        }

        $this->deleteMyFileRepository->execute($deleteMyFileParams);
    }
}