<?php
namespace App\UseCases\General\MyFile;

use App\Entities\General\MyFile\MyFileList;
use App\Repositories\General\MyFile\GetMyFilesRepositoryInterface as GetMyFilesRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class GetMyFilesUseCase
{
    /** @var GetMyFilesRepository $getMyFilesRepository */
    private $getMyFilesRepository;

    /**
     * @param GetMyFilesRepository $getMyFilesRepository
     */
    public function __construct(GetMyFilesRepository $getMyFilesRepository)
    {
        $this->getMyFilesRepository = $getMyFilesRepository;
    }

    /**
     * マイファイルリストをRepositoryから取得
     *
     * @param Request $request
     * @return array
     */
    public function execute(Request $request): array
    {
        $searchParams = [
            'my_id' => (int) Auth::id(),
            'from_date' => $request->input('fromDate'),
            'to_date' => $request->input('toDate')
        ];

        $myFiles = $this->getMyFilesRepository->execute($searchParams);

        return $this->conversion($myFiles);
    }

    /**
     * マイファイルリスト整形
     *
     * @param MyFileList $myFiles
     * @return array
     */
    private static function conversion(MyFileList $myFiles): array
    {
        $data = [];
        foreach ($myFiles as $myFile) {
            $data[] = [
                'id' => $myFile->getId(),
                'memo' => $myFile->getMemo(),
                'first_file_name' => basename($myFile->getFirstFilePath()),
                'first_file_path' => $myFile->getFirstFilePath(),
                'second_file_name' => basename($myFile->getSecondFilePath()),
                'second_file_path' => $myFile->getSecondFilePath(),
                'third_file_name' => basename($myFile->getThirdFilePath()),
                'third_file_path' => $myFile->getThirdFilePath(),
                'work_date' => $myFile->getWorkDate(),
                'work_place' => $myFile->getWorkPlace()
            ];
        }
        return $data;
    }
}