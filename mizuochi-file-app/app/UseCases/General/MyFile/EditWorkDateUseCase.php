<?php
namespace App\UseCases\General\MyFile;

use App\Repositories\General\MyFile\EditWorkDateRepositoryInterface as EditWorkDateRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

final class EditWorkDateUseCase extends MyFileBaseUseCase
{
    /** @var EditWorkDateRepository $editWorkDateRepository */
    private $editWorkDateRepository;

    /**
     * @param EditWorkDateRepository $editWorkDateRepository
     */
    public function __construct(EditWorkDateRepository $editWorkDateRepository)
    {
        $this->editWorkDateRepository = $editWorkDateRepository;
    }

    /**
     * ストレージファイルのディレクトリを変更
     * 作業日と変更したファイルパスの編集をReositoryに依頼
     *
     * @param Request $request
     */
    public function execute(Request $request)
    {
        $myId = (int) Auth::id();
        $changeWorkDate = $request->input('change_work_date');
        $currentFirstFilePath = $request->input('first_file_path');
        $currentSecondFilePath = $request->input('second_file_path');
        $currentThirdFilePath = $request->input('third_file_path');

        $editWorkDateParams = [
            'my_file_id' => $request->input('my_file_id'),
            'change_work_date' => $changeWorkDate
        ];

        $changeFirstFilePath = $this->changeFileDelectory($currentFirstFilePath, $myId, $changeWorkDate);
        $editWorkDateParams['change_first_file_path'] = $changeFirstFilePath;

        if (!is_null($currentSecondFilePath)) {
            $changeSecondFilePath = $this->changeFileDelectory($currentSecondFilePath, $myId, $changeWorkDate);
            $editWorkDateParams['change_second_file_path'] = $changeSecondFilePath;
        } else {
            $editWorkDateParams['change_second_file_path'] = null;
        }

        if (!is_null($currentThirdFilePath)) {
            $changeThirdFilePath = $this->changeFileDelectory($currentThirdFilePath, $myId, $changeWorkDate);
            $editWorkDateParams['change_third_file_path'] = $changeThirdFilePath;
        } else {
            $editWorkDateParams['change_third_file_path'] = null;
        }

        $this->editWorkDateRepository->execute($editWorkDateParams);
    }
}