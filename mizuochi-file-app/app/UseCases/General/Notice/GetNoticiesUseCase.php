<?php
namespace App\UseCases\General\Notice;

use App\Entities\Common\Notice\NoticeList;
use App\Repositories\General\Notice\GetNoticiesRepositoryInterface as GetNoticiesRepository;

final class GetNoticiesUseCase
{
    /** @var GetNoticiesRepository $getNoticiesRepository */
    private $getNoticiesRepository;

    /**
     * @param GetNoticiesUseCase $getNoticiesRepository
     */
    public function __construct(GetNoticiesRepository $getNoticiesRepository)
    {
        $this->getNoticiesRepository = $getNoticiesRepository;
    }

    /**
     * お知らせリストをRepositoryから取得
     *
     * @return array
     */
    public function execute(): array
    {
        $noticies = $this->getNoticiesRepository->execute();

        return $this->conversion($noticies);
    }

    /**
     * お知らせリストを整形
     *
     * @param NoticeList $noticies
     * @return array
     */
    private static function conversion(NoticeList $noticies): array
    {
        $data = [];
        foreach ($noticies as $notice) {
            $data[] = [
                'id' => $notice->getId(),
                'body' => $notice->getBody(),
                'created_date' => $notice->getCreatedDate()
            ];
        }
        return $data;
    }
}