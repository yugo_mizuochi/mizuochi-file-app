<?php
namespace App\Repositories\General\MyFile;

interface EditSecondWorkFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業ファイル2のパスを更新処理
     *
     * @param array $editSecondWorkFileParams
     */
    public function execute(array $editSecondWorkFileParams);
}