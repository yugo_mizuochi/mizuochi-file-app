<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class DeleteMyFileRepository implements DeleteMyFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルでマイファイル削除処理
     *
     * @param array $deleteMyFileParams
     */
    public function execute(array $deleteMyFileParams)
    {
        return DB::transaction(function () use($deleteMyFileParams) {
            $myFileModel = UserFileModel::find($deleteMyFileParams['my_file_id']);
            $myFileModel->delete();
        });
    }
}