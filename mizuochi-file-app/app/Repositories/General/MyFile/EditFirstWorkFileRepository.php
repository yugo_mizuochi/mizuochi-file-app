<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class EditFirstWorkFileRepository implements EditFirstWorkFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業ファイル1のパスを更新処理
     *
     * @param array $editFirstWorkFileParams
     */
    public function execute(array $editFirstWorkFileParams)
    {
        return DB::transaction(function () use($editFirstWorkFileParams) {
            $myFileModel = UserFileModel::find($editFirstWorkFileParams['my_file_id']);
            $myFileModel->first_file_path = $editFirstWorkFileParams['change_first_work_file_path'];
            $myFileModel->save();
        });
    }
}