<?php
namespace App\Repositories\General\MyFile;

interface EditWorkMemoRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業メモ編集処理
     *
     * @param array $editWorkMemoParams
     */
    public function execute(array $editWorkMemoParams);
}