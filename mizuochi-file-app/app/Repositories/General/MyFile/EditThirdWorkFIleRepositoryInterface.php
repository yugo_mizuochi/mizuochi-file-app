<?php
namespace App\Repositories\General\MyFile;

interface EditThirdWorkFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業ファイル3のパスを更新処理
     *
     * @param array $editThirdWorkFileParams
     */
    public function execute(array $editThirdWorkFileParams);
}