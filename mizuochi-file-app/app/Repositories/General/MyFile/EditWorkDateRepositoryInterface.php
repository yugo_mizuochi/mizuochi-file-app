<?php
namespace App\Repositories\General\MyFile;

interface EditWorkDateRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業日の更新処理
     *
     * @param array $editWorkDateParams
     */
    public function execute(array $editWorkDateParams);
}