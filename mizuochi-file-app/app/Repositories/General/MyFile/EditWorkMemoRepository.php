<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class EditWorkMemoRepository implements EditWorkMemoRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業メモ編集処理
     *
     * @param array $editWorkMemoParams
     */
    public function execute(array $editWorkMemoParams)
    {
        return DB::transaction(function () use($editWorkMemoParams) {
            $myFileModel = UserFileModel::find($editWorkMemoParams['my_file_id']);
            $myFileModel->memo = $editWorkMemoParams['change_work_memo'];
            $myFileModel->save();
        });
    }
}