<?php
namespace App\Repositories\General\MyFile;

interface EditFirstWorkFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業ファイル1のパスを更新処理
     *
     * @param array $editFirstWorkFileParams
     */
    public function execute(array $editFirstWorkFileParams);
}