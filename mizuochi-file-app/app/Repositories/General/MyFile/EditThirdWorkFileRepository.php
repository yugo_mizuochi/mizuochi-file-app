<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class EditThirdWorkFileRepository implements EditThirdWorkFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業ファイル3のパスを更新処理
     *
     * @param array $editThirdWorkFileParams
     */
    public function execute(array $editThirdWorkFileParams)
    {
        return DB::transaction(function ()  use($editThirdWorkFileParams) {
            $myFileModel = UserFileModel::find($editThirdWorkFileParams['my_file_id']);
            $myFileModel->third_file_path = $editThirdWorkFileParams['change_third_work_file_path'];
            $myFileModel->save();
        });
    }
}