<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class EditSecondWorkFileRepository implements EditSecondWorkFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業ファイル2のパスを更新処理
     *
     * @param array $editSecondWorkFileParams
     */
    public function execute(array $editSecondWorkFileParams)
    {
        return DB::transaction(function () use ($editSecondWorkFileParams) {
            $myFileModel = UserFileModel::find($editSecondWorkFileParams['my_file_id']);
            $myFileModel->second_file_path = $editSecondWorkFileParams['change_second_work_file_path'];
            $myFileModel->save();
        });
    }
}