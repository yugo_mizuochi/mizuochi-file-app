<?php
namespace App\Repositories\General\MyFile;

interface DeleteMyFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルでマイファイル削除処理
     *
     * @param array $deleteMyFileParams
     */
    public function execute(array $deleteMyFileParams);
}