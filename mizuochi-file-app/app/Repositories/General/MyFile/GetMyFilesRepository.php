<?php
namespace App\Repositories\General\MyFile;

use App\Entities\General\MyFile\MyFile;
use App\Entities\General\MyFile\MyFileList;
use App\Models\UserFile as UserFileModel;

final class GetMyFilesRepository implements GetMyFilesRepositoryInterface
{
    /** @var MyFileList $myFileList */
    private $myFileList;

    /**
     * @param MyFileList $myFileList
     */
    public function __construct(MyFileList $myFileList)
    {
        $this->myFileList = $myFileList;
    }

    /**
     * ユーザーファイルリスト取得処理
     *
     * @param array $searchParams
     * @return MyFileList
     */
    public function execute(array $searchParams): MyFileList
    {
        $myFiles = $this::getMyFilesFromModel(
            $searchParams['from_date'], 
            $searchParams['to_date'], 
            $searchParams['my_id']
        );

        foreach ($myFiles as $myFile) {
            $this->myFileList->add(MyFile::getInstance($myFile));
        }

        return $this->myFileList;
    }

    /**
     * ファイルモデルからマイファイル取得
     *
     * @param string $fromDate
     * @param string $toDate
     * @param integer $myId
     * @return object
     */
    private static function getMyFilesFromModel(
        string $fromDate = null, 
        string $toDate = null, 
        int $myId
    ): object
    {
        $myFiles = null;
        if (!empty($fromDate) && !empty($toDate)) {
            $myFiles = UserFileModel::where('user_id', $myId)
                ->whereBetween('work_date', [$fromDate, $toDate])
                ->orderBy('work_date')
                ->get();
        } else if (!empty($fromDate) && empty($toDate)) {
            $myFiles = UserFileModel::where('user_id', $myId)
                ->whereDate('work_date', $fromDate)
                ->orderBy('work_date')
                ->get();
        } else if (empty($fromDate) && !empty($toDate)) {
            $myFiles = UserFileModel::where('user_id', $myId)
                ->whereDate('work_date', $toDate)
                ->orderBy('work_date')
                ->get();
        } else {
            $myFiles = UserFileModel::where('user_id', $myId)
                ->orderBy('work_date')
                ->get();
        }
        return $myFiles;
    }
}