<?php
namespace App\Repositories\General\MyFile;

interface AddMyFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに新規登録処理
     *
     * @param array $addParams
     */
    public function execute(array $addParams);
}