<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class EditWorkPlaceRepository implements EditWorkPlaceRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業場所編集処理
     *
     * @param array $editWorkPlaceParams
     */
    public function execute(array $editWorkPlaceParams)
    {
        return DB::transaction(function () use($editWorkPlaceParams) {
            $myFileModel = UserFileModel::find($editWorkPlaceParams['my_file_id']);
            $myFileModel->work_place = $editWorkPlaceParams['change_work_place'];
            $myFileModel->save();
        });
    }
}