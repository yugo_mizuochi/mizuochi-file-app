<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class AddMyFileRepository implements AddMyFileRepositoryInterface
{
    /**
     * ユーザーファイルモデルに新規登録処理
     *
     * @param array $addParams
     */
    public function execute(array $addParams)
    {
        return DB::transaction(function () use ($addParams) {
            $myFileModel = new UserFileModel();
            $myFileModel->user_id = $addParams['my_id'];
            $myFileModel->memo = $addParams['memo'];
            $myFileModel->first_file_path = $addParams['first_file_path'];
            $myFileModel->second_file_path = $addParams['second_file_path'];
            $myFileModel->third_file_path = $addParams['third_file_path'];
            $myFileModel->work_date = $addParams['work_date'];
            $myFileModel->work_place = $addParams['work_place'];
            $myFileModel->save();
        });
    }
}