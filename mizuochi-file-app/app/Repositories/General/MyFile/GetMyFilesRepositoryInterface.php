<?php
namespace App\Repositories\General\MyFile;

use App\Entities\General\MyFile\MyFileList;

interface GetMyFilesRepositoryInterface
{
    /**
     * ユーザーファイルリスト取得処理
     *
     * @param array $searchParams
     * @return MyFileList
     */
    public function execute(array $searchParams): MyFileList;
}