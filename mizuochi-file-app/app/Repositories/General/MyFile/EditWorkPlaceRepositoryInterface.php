<?php
namespace App\Repositories\General\MyFile;

interface EditWorkPlaceRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業場所編集処理
     *
     * @param array $editWorkPlaceParams
     */
    public function execute(array $editWorkPlaceParams);
}