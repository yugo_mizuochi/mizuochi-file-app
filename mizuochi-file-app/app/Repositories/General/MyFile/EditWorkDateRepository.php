<?php
namespace App\Repositories\General\MyFile;

use App\Models\UserFile as UserFileModel;
use Illuminate\Support\Facades\DB;

final class EditWorkDateRepository implements EditWorkDateRepositoryInterface
{
    /**
     * ユーザーファイルモデルに作業日の更新処理
     *
     * @param array $editWorkDateParams
     */
    public function execute(array $editWorkDateParams)
    {
        return DB::transaction(function () use($editWorkDateParams) {
            $myFileModel = UserFileModel::find($editWorkDateParams['my_file_id']);
            $myFileModel->first_file_path = $editWorkDateParams['change_first_file_path'];
            $myFileModel->second_file_path = $editWorkDateParams['change_second_file_path'];
            $myFileModel->third_file_path = $editWorkDateParams['change_third_file_path'];
            $myFileModel->work_date = $editWorkDateParams['change_work_date'];
            $myFileModel->save();
        });
    }
}