<?php
namespace App\Repositories\General\MyAccount;

use App\Entities\General\MyAccount\MyAccount;

interface GetMyAccountRepositoryInterface
{
    /**
     * マイアカウント取得処理
     *
     * @param array $getMyAccountParams
     * @return MyAccount
     */
    public function execute(array $getMyAccountParams): MyAccount;
}