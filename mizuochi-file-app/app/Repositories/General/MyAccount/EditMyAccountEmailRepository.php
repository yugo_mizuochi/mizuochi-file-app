<?php
namespace App\Repositories\General\MyAccount;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

final class EditMyAccountEmailRepository implements EditMyAccountEmailRepositoryInterface
{
    /**
     * ユーザーファイルモデルにメールアドレス編集処理
     *
     * @param array $editMyAccountEmailParams
     */
    public function execute(array $editMyAccountEmailParams)
    {
        return DB::transaction(function () use($editMyAccountEmailParams) {
            $myAccountModel = UserModel::find($editMyAccountEmailParams['my_account_id']);
            $myAccountModel->email = $editMyAccountEmailParams['change_my_account_email'];
            $myAccountModel->save();
        });
    }
}