<?php
namespace App\Repositories\General\MyAccount;

interface EditMyAccountEmailRepositoryInterface
{
    /**
     * ユーザーファイルモデルにメールアドレス編集処理
     *
     * @param array $editMyAccountEmailParams
     */
    public function execute(array $editMyAccountEmailParams);
}