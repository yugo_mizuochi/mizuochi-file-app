<?php
namespace App\Repositories\General\MyAccount;

use App\Entities\General\MyAccount\MyAccount;
use App\Models\User as UserModel;

final class GetMyAccountRepository implements GetMyAccountRepositoryInterface
{
    /**
     * マイアカウント取得処理
     *
     * @param array $getMyAccountParams
     * @return MyAccount
     */
    public function execute(array $getMyAccountParams): MyAccount
    {
        $myAccountModel = UserModel::find($getMyAccountParams['my_id']);

        $myFilesModels = $myAccountModel->userFiles()->whereDate('work_date', date('Y-m-d'))->get();

        $todayWorkPlaces = [];

        foreach ($myFilesModels as $myFileModel) {
            $todayWorkPlaces[] = $myFileModel->work_place;
        }

        return MyAccount::getInstace($myAccountModel, $todayWorkPlaces);
    }
}