<?php
namespace App\Repositories\General\MyAccount;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

final class EditMyAccountNameRepository implements EditMyAccountNameRepositoryInterface
{
    /**
     * ユーザーモデルに名前編集処理
     *
     * @param array $editMyAccountNameParams
     */
    public function execute(array $editMyAccountNameParams)
    {
        return DB::transaction(function () use($editMyAccountNameParams) {
            $myAccountModel = UserModel::find($editMyAccountNameParams['my_account_id']);
            $myAccountModel->name = $editMyAccountNameParams['change_my_account_name'];
            $myAccountModel->save();
        });
    }
}