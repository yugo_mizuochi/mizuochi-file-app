<?php
namespace App\Repositories\General\MyAccount;

interface EditMyAccountNameRepositoryInterface
{
    /**
     * ユーザーモデルに名前編集処理
     *
     * @param array $editMyAccountNameParams
     */
    public function execute(array $editMyAccountNameParams);
}