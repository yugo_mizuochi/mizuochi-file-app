<?php
namespace App\Repositories\General\Notice;

use App\Entities\Common\Notice\NoticeList;

interface GetNoticiesRepositoryInterface
{
    /**
     * お知らせリスト取得処理
     *
     * @return NoticeList
     */
    public function execute(): NoticeList;
}