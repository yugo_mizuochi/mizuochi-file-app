<?php
namespace App\Repositories\Admin\Notice;

use App\Models\Notice as NoticeModel;
use Illuminate\Support\Facades\DB;

final class EditNoticeBodyRepository implements EditNoticeBodyRepositoryInterface
{
    /**
     * お知らせmodelに本文編集処理
     *
     * @param array $editNoticeBodyParams
     */
    public function execute(array $editNoticeBodyParams)
    {
        return DB::transaction(function () use($editNoticeBodyParams) {
            $noticeModel = NoticeModel::find($editNoticeBodyParams['notice_id']);
            $noticeModel->body = $editNoticeBodyParams['change_notice_body'];
            $noticeModel->save();
        });
    }
}