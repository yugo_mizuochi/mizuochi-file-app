<?php
namespace App\Repositories\Admin\Notice;

use App\Models\Notice as NoticeModel;
use Illuminate\Support\Facades\DB;

final class AddNoticeRepository implements AddNoticeRepositoryInterface
{
    /**
     * お知らせモデルにお知らせ新規登録処理
     *
     * @param array $addNoticeParams
     */
    public function execute(array $addNoticeParams)
    {
        return DB::transaction(function () use($addNoticeParams) {
            $noticeModel = new NoticeModel();
            $noticeModel->body = $addNoticeParams['notice_body'];
            $noticeModel->save();
        });
    }
}