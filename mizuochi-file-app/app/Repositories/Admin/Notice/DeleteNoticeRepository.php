<?php
namespace App\Repositories\Admin\Notice;

use App\Models\Notice as NoticeModel;
use Illuminate\Support\Facades\DB;

final class DeleteNoticeRepository implements DeleteNoticeRepositoryInterface
{
    /**
     * お知らせmodelに削除処理
     *
     * @param array $deleteNoticeParams
     */
    public function execute(array $deleteNoticeParams)
    {
        return DB::transaction(function () use($deleteNoticeParams) {
            $noticeModel = NoticeModel::find($deleteNoticeParams['delete_notice_id']);
            $noticeModel->delete();
        });
    }
}