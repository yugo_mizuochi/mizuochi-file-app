<?php
namespace App\Repositories\Admin\Notice;

use App\Entities\Common\Notice\Notice;
use App\Entities\Common\Notice\NoticeList;
use App\Models\Notice as NoticeModel;

final class GetNoticiesRepository implements GetNoticiesRepositoryInterface
{
    /** @var NoticeList $noticeList */
    private $noticeList;

    /**
     * @param NoticeList $noticeList
     */
    public function __construct(NoticeList $noticeList)
    {
        $this->noticeList = $noticeList;
    }

    /**
     * お知らせリスト取得処理
     *
     * @return NoticeList
     */
    public function execute(): NoticeList
    {
        $noticeModels = NoticeModel::orderBy('id', 'desc')->get();

        foreach ($noticeModels as $noticeModel) {
            $this->noticeList->add(Notice::getInstance($noticeModel));
        }

        return $this->noticeList;
    }
}