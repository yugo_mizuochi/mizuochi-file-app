<?php
namespace App\Repositories\Admin\Notice;

interface AddNoticeRepositoryInterface
{
    /**
     * お知らせモデルにお知らせ新規登録処理
     *
     * @param array $addNoticeParams
     */
    public function execute(array $addNoticeParams);
}