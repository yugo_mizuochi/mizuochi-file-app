<?php
namespace App\Repositories\Admin\Notice;

use App\Entities\Common\Notice\NoticeList;

interface GetNoticiesRepositoryInterface
{
    /**
     * お知らせリスト取得処理
     *
     * @return NoticeList
     */
    public function execute(): NoticeList;
}