<?php
namespace App\Repositories\Admin\Notice;


interface EditNoticeBodyRepositoryInterface
{
    /**
     * お知らせmodelに本文編集処理
     *
     * @param array $editNoticeBodyParams
     */
    public function execute(array $editNoticeBodyParams);
}