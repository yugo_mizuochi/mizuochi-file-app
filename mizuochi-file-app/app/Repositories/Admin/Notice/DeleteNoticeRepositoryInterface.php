<?php
namespace App\Repositories\Admin\Notice;

interface DeleteNoticeRepositoryInterface
{
    /**
     * お知らせmodelに削除処理
     *
     * @param array $deleteNoticeParams
     */
    public function execute(array $deleteNoticeParams);
}