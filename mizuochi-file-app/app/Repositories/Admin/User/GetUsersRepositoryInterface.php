<?php
namespace App\Repositories\Admin\User;

use App\Entities\Admin\Users\UserList;

interface GetUsersRepositoryInterface
{
    /**
     * ユーザーリスト取得処理
     *
     * @return UserList
     */
    public function execute(): UserList;
}