<?php
namespace App\Repositories\Admin\User;

interface EditUserNameRepositoryInterface
{
    /**
     * ユーザーモデルにユーザー名編集処理
     *
     * @param array $editUserNameParams
     */
    public function execute(array $editUserNameParams);
}