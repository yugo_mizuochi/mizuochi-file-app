<?php
namespace App\Repositories\Admin\User;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

final class EditUserEmailRepository implements EditUserEmailRepositoryInterface
{
    /**
     * ユーザーモデルにメールアドレス編集処理
     *
     * @param array $editUserEmailParams
     */
    public function execute(array $editUserEmailParams)
    {
        return DB::transaction(function () use($editUserEmailParams) {
            $userModel = UserModel::find($editUserEmailParams['user_id']);
            $userModel->email = $editUserEmailParams['change_user_email'];
            $userModel->save();
        });
    }
}