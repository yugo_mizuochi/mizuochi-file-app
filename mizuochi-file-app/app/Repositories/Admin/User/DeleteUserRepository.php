<?php
namespace App\Repositories\Admin\User;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

final class DeleteUserRepository implements DeleteUserRepositoryInterface
{
    /**
     * modelにてユーザーファイルとユーザー削除
     *
     * @param integer $userId
     */
    public function execute(int $userId)
    {
        return DB::transaction(function () use($userId) {
            $userModel = UserModel::find($userId);
            $userFileModels = $userModel->userFiles;
            foreach ($userFileModels as $userFileModel) {
                $userFileModel->delete();
            }
            $userModel->delete();
        });
    }
}
