<?php
namespace App\Repositories\Admin\User;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

final class AddUserRepository implements AddUserRepositoryInterface
{
    /**
     * ユーザーモデルに新規登録処理
     *
     * @param array $addUserParams
     */
    public function execute(array $addUserParams)
    {
        return DB::transaction(function () use($addUserParams) {
            $userModel = new UserModel();
            $userModel->name = $addUserParams['user_name'];
            $userModel->email = $addUserParams['user_email'];
            $userModel->password = Hash::make($addUserParams['user_password']);
            $userModel->role = $addUserParams['user_role'];
            $userModel->save();
        });
    }
}