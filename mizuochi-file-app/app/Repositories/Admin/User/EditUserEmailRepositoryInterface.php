<?php
namespace App\Repositories\Admin\User;

interface EditUserEmailRepositoryInterface
{
    /**
     * ユーザーモデルにメールアドレス編集処理
     *
     * @param array $editUserEmailParams
     */
    public function execute(array $editUserEmailParams);
}