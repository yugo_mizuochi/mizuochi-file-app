<?php
namespace App\Repositories\Admin\User;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

final class EditUserRoleRepository implements EditUserRoleRepositoryInterface
{
    /**
     * ユーザーモデルにユーザー権限編集処理
     *
     * @param array $editUserRoleParams
     */
    public function execute(array $editUserRoleParams)
    {
        return DB::transaction(function () use($editUserRoleParams) {
            $userModel = UserModel::find($editUserRoleParams['user_id']);
            $userModel->role = $editUserRoleParams['change_user_role'];
            $userModel->save();
        });
    }
}