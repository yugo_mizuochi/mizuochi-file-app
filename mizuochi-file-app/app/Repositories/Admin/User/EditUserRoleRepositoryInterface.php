<?php
namespace App\Repositories\Admin\User;

interface EditUserRoleRepositoryInterface
{
    /**
     * ユーザーモデルにユーザー権限編集処理
     *
     * @param array $editUserRoleParams
     */
    public function execute(array $editUserRoleParams);
}