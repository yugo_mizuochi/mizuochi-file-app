<?php
namespace App\Repositories\Admin\User;

use App\Enums\UserRole;

final class GetUserRolesRepository implements GetUserRolesRepositoryInterface
{
    /**
     * ユーザー権限リストを取得
     *
     * @return array
     */
    public function execute(): array
    {
        return UserRole::getUserRoles();
    }
}