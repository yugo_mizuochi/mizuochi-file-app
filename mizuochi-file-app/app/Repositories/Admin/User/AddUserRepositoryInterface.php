<?php
namespace App\Repositories\Admin\User;

interface AddUserRepositoryInterface
{
    /**
     * ユーザーモデルに新規登録処理
     *
     * @param array $addUserParams
     */
    public function execute(array $addUserParams);
}