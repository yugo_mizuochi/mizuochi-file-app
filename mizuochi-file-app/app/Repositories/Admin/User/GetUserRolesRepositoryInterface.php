<?php
namespace App\Repositories\Admin\User;

interface GetUserRolesRepositoryInterface
{
    /**
     * ユーザー権限リストを取得
     *
     * @return array
     */
    public function execute(): array;
}