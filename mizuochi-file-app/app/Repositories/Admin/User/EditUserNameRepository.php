<?php
namespace App\Repositories\Admin\User;

use App\Models\User as UserModel;
use Illuminate\Support\Facades\DB;

final class EditUserNameRepository implements EditUserNameRepositoryInterface
{
    /**
     * ユーザーモデルにユーザー名編集処理
     *
     * @param array $editUserNameParams
     */
    public function execute(array $editUserNameParams)
    {
        return DB::transaction(function () use($editUserNameParams) {
            $userModel = UserModel::find($editUserNameParams['user_id']);
            $userModel->name = $editUserNameParams['change_user_name'];
            $userModel->save();
        });
    }
}