<?php
namespace App\Repositories\Admin\User;

interface DeleteUserRepositoryInterface
{
    /**
     * modelにてユーザーファイルとユーザー削除
     *
     * @param integer $userId
     */
    public function execute(int $userId);
}