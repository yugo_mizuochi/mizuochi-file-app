<?php
namespace App\Repositories\Admin\User;

use App\Entities\Admin\Users\User;
use App\Entities\Admin\Users\UserList;
use App\Models\User as UserModel;

final class GetUsersRepository implements GetUsersRepositoryInterface
{
    /** @var UserList $userList */
    private $userList;

    /**
     * @param UserList $userList
     */
    public function __construct(UserList $userList)
    {
        $this->userList = $userList;
    }

    /**
     * ユーザーリスト取得処理
     *
     * @return UserList
     */
    public function execute(): UserList
    {
        $userModels = UserModel::orderBy('id', 'desc')->get();

        foreach ($userModels as $userModel) {
            $this->userList->add(User::getInstace($userModel));
        }

        return $this->userList;
    }
}