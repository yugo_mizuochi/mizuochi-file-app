<?php

namespace App\Enums;

final class UserRole
{
    /** @var array $userRoles */
    private static $userRoles = [
        'general',
        'admin'
    ];

    /**
     * ユーザータイプ判別
     *
     * @param string $userRole
     * @return string
     */
    private static function judgeUserRole(string $userRole): string
    {
        switch ($userRole) {
            case 'admin':
                return '管理者';
                break;
            
            case 'general':
                return '一般';
                break;
        }
    }

    /**
     * ユーザータイプリスト取得
     *
     * @return array
     */
    public static function getUserRoles(): array
    {
        $userRoles = array_map(function($userRole) {
            return [
                'name' => self::judgeUserRole($userRole),
                'value' => $userRole
            ];
        }, static::$userRoles);
        
        return $userRoles;
    }
}
