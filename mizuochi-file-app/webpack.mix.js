const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/common/common.scss', 'public/css/common')
   .sass('resources/sass/general/home/home.scss', 'public/css/general/home')
   .sass('resources/sass/general/myfiles/myfiles.scss', 'public/css/general/myfiles')
   .sass('resources/sass/general/myaccount/myaccount.scss', 'public/css/general/myaccount')
   .sass('resources/sass/admin/noticies/noticies.scss', 'public/css/admin/noticies')
   .sass('resources/sass/admin/users/users.scss', 'public/css/admin/users');
