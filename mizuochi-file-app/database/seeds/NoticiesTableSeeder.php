<?php

use Illuminate\Database\Seeder;

class NoticiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('noticies')->insert([
        	[
        		'body' => 'リリースしました。',
                'created_at' => date('Y/m/d')
        	],
    	]);
    }
}
