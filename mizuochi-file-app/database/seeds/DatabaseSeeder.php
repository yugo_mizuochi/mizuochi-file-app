<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (\App::environment('local')) {
            $seeds = [
                UsersTableSeeder::class,
                NoticiesTableSeeder::class,
                UserFilesTableSeeder::class,
            ];
        } else {
            $seeds = [
                NoticiesTableSeeder::class,
                ProductionUsersTableSeeder::class,
            ];
        }
        $this->call($seeds);
    }
}
