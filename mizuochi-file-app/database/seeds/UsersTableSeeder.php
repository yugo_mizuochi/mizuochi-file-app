<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
	        [
	        	'name' => 'Mizuochi Yugo',
                'email' => 'elric.mizuochi@i.softbank.jp',
                'email_verified_at' => null,
                'password' => Hash::make('Mizuochi5525'),
                'role' => 'admin',
	        	'remember_token' => null,
	        	'created_at' => date('Y/m/d')
	        ],
	        [
	        	'name' => 'テスト太郎',
                'email' => 'test@co.jp',
                'email_verified_at' => null,
                'password' => Hash::make('password'),
                'role' => 'general',
	        	'remember_token' => null,
	        	'created_at' => date('Y/m/d')	
	        ],
        ]);
    }
}
