<?php

use Illuminate\Database\Seeder;

class UserFilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_files')->insert([
        	[
                'user_id' => 1,
                'memo' => 'サンプルテキスト',
                'first_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test.txt',
                'second_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test2.txt',
                'third_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test3.txt',
                'work_date' => date('Y/m/d'),
                'work_place' => '作業場1',
                'created_at' => date('Y/m/d')
        	],
        	[
                'user_id' => 1,
                'memo' => 'サンプルテキスト',
                'first_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test.txt',
                'second_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test2.txt',
                'third_file_path' => null,
                'work_date' => date('Y/m/d'),
                'work_place' => '作業場2',
                'created_at' => date('Y/m/d')
            ],
            [
                'user_id' => 1,
                'memo' => 'サンプルテキスト',
                'first_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test.txt',
                'second_file_path' => null,
                'third_file_path' => null,
                'work_date' => date('Y/m/d'),
                'work_place' => '作業場3',
                'created_at' => date('Y/m/d')
            ],
            [
                'user_id' => 2,
                'memo' => 'サンプルテキスト',
                'first_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test.txt',
                'second_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test2.txt',
                'third_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test3.txt',
                'work_date' => date('Y/m/d'),
                'work_place' => '作業場1',
                'created_at' => date('Y/m/d')
        	],
        	[
                'user_id' => 2,
                'memo' => 'サンプルテキスト',
                'first_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test.txt',
                'second_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test2.txt',
                'third_file_path' => null,
                'work_date' => date('Y/m/d'),
                'work_place' => '作業場2',
                'created_at' => date('Y/m/d')
            ],
            [
                'user_id' => 2,
                'memo' => 'サンプルテキスト',
                'first_file_path' => 'https://file-management-app-bucket.s3-ap-northeast-1.amazonaws.com/test/test.txt',
                'second_file_path' => null,
                'third_file_path' => null,
                'work_date' => date('Y/m/d'),
                'work_place' => '作業場3',
                'created_at' => date('Y/m/d')
            ],
    	]);
    }
}
